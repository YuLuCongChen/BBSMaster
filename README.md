# 一、概述

## 1.1 选题背景

在二十一世纪的今天，随着互联网技术的迅猛发展，网络给人们带来了很多便利，  比如人们借助于网络进行相互交流变得更加方便。因此，交流工具作为互联网中运用最为广泛的通信工具之一。

校园BBS 也就是我们常说的校园论坛，BBS 是英文 Bulletin Board System 的简称。校园BBS 是通过网络社区，提供给人们一个虚拟的空间释放压力。现代生活中，无论是成年人还是未成年人都面临巨大的压力，在网络这个没有任何压力的世界，人们能够更好地放松自己，以便更好地投入现实生活。虚拟的社区能够创造一个全新的展现自我的舞台，能够在其中做许多平时不能完成的事情，发泄一下内心的郁闷。校园 BBS 同时也是一个信息的港湾，它集结了许许多多的内容，在这里青少年可以接触到许多平时难以接触的信息。当代大学生喜爱结交新朋友，寻找有共同兴趣的人交流讨论，校园 BBS 可以为这些有共同爱好的年轻人创造另一片交流的空间。

校园BBS 系统是基于 WEB 网页，无需专用客户端即可为用户提供简洁的聊天交流界面，用户通过发布话题以及回帖的方式在论坛中进行学习生活等方面的交流，结合管理员在线对论坛信息进行实时调整管理，从而成为最为稳定的一个虚拟的校园交流平台。  可以提供论坛的发表话题、发布留言、站内搜索、论坛管理、修改密码、注册账户等功能。

## 1.2设计内容

完成具体功能：

- 用户登录，利用数据库检查输入身份是否正确。

- 板块管理，当管理员登录成功后，通过该功能模块进入板块管理，可以增加删除  板块。

- 话题管理，在此界面可以对话题进行增删改查。

- 留言管理，在此界面可以对留言进行增删改查，用户自己发的留言只能管理员和  自己删除，其他人不能删除。

- 用户管理，此功能可以对普通用户维护，如删除用户。

  

## 1.3设计要求

- 正确理解题意；

- 具备良好的编程规范和适当的注释；

- 通过调试、运行程序，加强对 web 设计的理解；

- 按要求编写课程设计报告书。

# 二、系统需求分析

## 2.1引言

- 系统整体描述

该系统为校园 BBS 系统，学生注册后可登陆系统。学生登录校园论坛系统可以查看相关模块、查询相关模块、查看话题、查询话题、删除所发表话题、查看留言、发表留言、删除所发表留言以及修改密码；管理员登录校园论坛系统后除了可以使用普通用户的所有功能   之外，还可以使用删除用户、增加管理员、增加模块、删除任意模块、删除任意话题和删除   任意留言等相关功能。

- 软件项目约束

1. 时间约束：系统的开发需要一定的时间，按照开发进度计划，大概需要一周的开发时间，后期还需要修改和调试完善的时间。
2. 技术约束：本系统采用 jsp/js/servlet/java+MySQL 的框架结构进行开发，需要电脑支持Windows 7/Windows 10 系统，开发软件主要有 Eclipse 和 Mysql 等，这些开发软件需要通过合法手段获取。

## 2.2定义系统

校园 BBS 系统



## 2.3确定执行者

- 使用系统的人：学生、管理员

- 与系统交互的其他系统：用户信息管理系统、话题信息管理系统、留言信息管理系统、模块信息管理系统。

**执行者及其简要描述：**

- 学生：使用校园 BBS 系统的人

- 管理员：管理论坛相关信息及用户的人

- 用户信息管理系统：创建和维护用户信息的软件系统

- 话题信息管理系统：创建和维护话题信息的软件系统

- 留言信息管理系统：创建和维护留言信息的软件系统

- 模块信息管理系统：创建和维护模块信息的软件系统

  

  

## 2.4信息描述

- 信息内容表示

本系统主要要解决的问题是：学生登录校园论坛系统可以查看相关模块、查询相关模块、   查看话题、查询话题、删除所发表话题、查看

留言、发表留言、删除所发表留言以及修改密码；管理员登录校园论坛系统后除了可以使用普通用户的所有功能之外，还可以使用删除用

户、增加管理员、增加模块、删除任意模块、删除任意话题和删除任意留言等相关功能。

- 信息流表示

  a）登录信息

  b）模块信息

  c）话题信息

  d）留言信息

  e）状态信息

## 2.5功能描述

- 功能划分

系统功能可划分为：注册/登录、查询模块、增加模块、删除模块、查询话题、发布话题、删除话题、发布留言、删除留言、添加管理员、删除用户、密码修改。

- 功能模块描述

注册/登录:为用户提供注册和登录的功能，登陆后可使用系统。b） 查询模块：为用户提供模块查询功能，可以查询感兴趣的模块。c） 添加模块：管理员进入功能操作选项后即可添加相关模块。

1. 删除模块：管理员进入功能操作选项后即可删除相关模块。
2. 查询话题：在学生进入相应模块后可查询相关话题。
3. 发布话题：在学生进入相应模块后可发布相关话题。
4. 删除话题：学生只可删除自己发布的话题，管理员可以删任意话题。
5. 发布留言：在学生进入相应话题后可发布相关留言。
6. 删除留言：学生只可删除自己发布的留言，管理员可以删任意留言。
7. 添加管理员：管理员进入用户管理界面后输入用户名密码确认密码后即可添加管理员。
8. 删除用户：管理员进入用户管理界面后输入用户名即可删除用户。
9. 修改密码：用户管理员登陆后皆可修改密码。

## 2.6行为描述

**事件和响应**

- 注册学生，输入 id、密码，注册成功。

- 学生、管理员登录，输入 id，密码正确则登录成功。

- 查询模块，输入关键字查询相关模块，显示相应模块则查询成功。

- 增加模块，输入模块名添加模块，在查看所有模块中显示相应模块即为添加成功。

- 删除模块，点击模块名下的删除按钮，得到响应后，该模块不再显示；

- 查询话题，输入关键字查询相关话题，显示相应话题查询成功。

- 发布话题， 在相应的模块选择添加话题，输入自己的话题内容并显示即为添加成功。

- 删除话题，点击话题名下的删除按钮，得到响应后，该话题不再显示；

- 发布留言，在相应的话题下面留言，输入留言内容并显示即为成功

- 删除留言，点击留言下的删除按钮，得到响应后，该留言不再显示；

- 添加管理员，输入用户名密码以及确认密码确认无误后用相应的用户名密码登录成功则添加管理员成功。

- 删除用户，输入用户名即可删除用户，可以通过数据库查询该用户不存在即为删除用户成功。

- 修改密码，用户和管理员皆可修改密码，修改后使用修改的密码重新登陆成功  即为修改成功。

  

  

## 2.7系统用例图

![](https://www.writebug.com/myres/static/uploads/2021/10/26/5203f314c4ea14320d3d03a8321fdc15.writebug)

图 2.1 系统用例图

# 三、系统总体设计

## 3.1系统总体功能结构

![](https://www.writebug.com/myres/static/uploads/2021/10/26/b19e3b3f93a599d71bcadf9dbdd50d24.writebug)

图 3.1 系统功能结构图

补充：除上述功能外，管理员和普通用户都具有话题、留言的删除功能，管理员能删除所有   人的留言，普通用户只能删除自己发布的话题或者留言。

## 3.2模块功能设计

功能在 servlet 里调用封装好的函数来实现，现附上主要代码。

### 3.2.1登录功能

```c++
protected void doPost(HttpServletRequest request, HttpServletResponse response)
throws ServletException, IOException
{
    // TODO Auto-generated method stub response.setContentType("text/html;charset=UTF-8"); request.setCharacterEncoding("UTF-8");
    PrintWriter out = response.getWriter();
    byte b1[] = request.getParameter("uname").getBytes("UTF-8");
    String userid = new String(b1, "UTF-8");
    byte b2[] = request.getParameter("pwd").getBytes("UTF-8");
    String pwd = new String(b2);
    byte b3[] = request.getParameter("identity").getBytes("UTF-8");
    String identity = new String(b3);
    if(DB.getDb().checkUser(userid, pwd, identity) == null) //进行身份验证，如果返回null说明登录失败，转回登陆界面
    {
        HttpSession session = request.getSession();
        session.setAttribute("message", "登录失败！");
        response.sendRedirect("login.jsp");
    }
    else
    {
        HttpSession session = request.getSession();
        session.setAttribute("userid", userid); //登陆成功，在session中存入
        已经登陆用户名
        session.setAttribute("identity", identity); //登陆成功，在session中
        存入身份
        Cookie cookie1 = new Cookie("uname", userid); //用户名和密码存入cookie Cookie cookie2=new Cookie("pwd",pwd); response.addCookie(cookie1);
        response.addCookie(cookie2);
        session.setAttribute("success", " 登 陆 成 功 ！");
        session.setAttribute("message", null);
        if(session.getAttribute("identity").equals("admin"))
        {
            response.sendRedirect("Homepage_admin.jsp");
        }
        else
        {
            response.sendRedirect("Homepage_user.jsp");
        }
    }
}
```



### 3.2.2注册功能

```c++
protected void doPost(HttpServletRequest request, HttpServletResponse response)
throws ServletException, IOException
{
    // TODO Auto-generated method stub response.setContentType("text/html;charset=UTF-8"); PrintWriter out=response.getWriter(); request.setCharacterEncoding("UTF-8");
    byte b1[] = request.getParameter("uname").getBytes("UTF-8");
    String userid = new String(b1, "UTF-8");
    byte b2[] = request.getParameter("pwd").getBytes("UTF-8");
    String pwd = new String(b2);
    byte b3[] = request.getParameter("qpwd").getBytes("UTF-8");
    String qpwd = new String(b3);
    if(pwd.equals(qpwd))
    {
        DB.getDb().insertUser(userid, pwd); //将注册信息插入数据库out.print("注册成功，两秒后跳转至登录界面！！！"); response.setHeader("Refresh","2;login.jsp");
    }
    else
    {
        out.print("注册失败，请检查用户名或密码是否正确，两秒后跳转至登录页
            面！！！ ");
        }
    }
```

### 3.2.3 添加模块、话题、留言

```c++
if(request.getParameter("mname") != null) //插入模块
{
    模块名
}
byte b1[] = request.getParameter("mname").getBytes("UTF-8"); //获取
String content = new String(b1, "UTF-8");
Module m = new Module();
m.setMname(content);
m.setMnum(Math.abs((int) new Date().getTime()));
DB.getDb().addModule(m); //模块插入数据库response.sendRedirect("result?pos=module");//刷新
if(request.getParameter("tname") != null) //插入话题
{
    话题名
}
byte b1[] = request.getParameter("tname").getBytes("UTF-8"); //获取
String content = new String(b1, "UTF-8");
Topic m = new Topic();
m.setTname(content);
m.setOwner((String) session.getAttribute("userid"));
m.setTnum(Math.abs((int) new Date().getTime()));
m.setMnum((int) session.getAttribute("mnum"));
DB.getDb().addTopic(m); //话题插入数据库response.sendRedirect("result?pos=topic");//刷新
言内容
if(request.getParameter("content") != null) //插入留言
{
    byte b1[] = request.getParameter("content").getBytes("UTF-8"); //获取留
    String content = new String(b1, "UTF-8");
    Message m = new Message();
    m.setContent(content);
    m.setDate(new Date());
    m.setUserid((String) session.getAttribute("userid")); //通过session获取登陆用户id
    m.setMnum((int) session.getAttribute("mnum"));
    m.setTnum((int) session.getAttribute("tnum"));
    DB.getDb().addInfo(m); //消息插入数据库response.sendRedirect("result?pos=message");//刷新
}
```

### 3.2.4 删除模块、话题、留言及用户

```c++

```

### 3.2.5 显示模块、话题、留言

```c++
protected void doGet(HttpServletRequest request, HttpServletResponse response)
throws ServletException, IOException
{
    // TODO Auto-generated method stub
    //response.getWriter().append("Served at: ").append(request.getContextPath());
    response.setContentType("text/html;charset=UTF-8");
    PrintWriter out = response.getWriter();
    String m_id = request.getParameter("m_id");
    String tnum = request.getParameter("tnum");
    String mnum = request.getParameter("mnum");
    String userid = request.getParameter("userid");
    if(m_id != null)
    {
        DB.getDb().delete(0, m_id); //删除消息response.sendRedirect("result?pos=message");
    }
    if(tnum != null)
    {
        DB.getDb().delete(1, tnum); //删除话题response.sendRedirect("result?pos=topic");
    }
    if(mnum != null)
    {
        DB.getDb().delete(2, mnum); //删除模块response.sendRedirect("result?pos=module");
    }
    if(userid != null)
    {
        DB.getDb().delete(3, userid); //删除用户out.print("删除成功，两秒后跳转！！！");
        response.setHeader("Refresh", "2;usermanagement.jsp");
    }
}
```



### 3.2.6 查询模块、话题、留言

```c++
protected void doPost(HttpServletRequest request, HttpServletResponse response)
throws ServletException, IOException {
// TODO Auto-generated method stub response.setContentType("text/html;charset=UTF-8"); request.setCharacterEncoding("UTF-8");
//response.setContentType("text/html;charset=UTF-8"); HttpSession  session=request.getSession(); PrintWriter out=response.getWriter();
String sname=request.getParameter("sname"); String tname=request.getParameter("tname");
//System.out.println(sname);
try{
if(tname!=null) { ArrayList<Topic> t; t=DB.getDb().search(tname,1);
session.setAttribute("result",t);//模块队列存入session
response.setHeader("Refresh","2;topic.jsp");//转到话题显示
}
if(sname!=null){ ArrayList<Module> t; t=DB.getDb().search(sname,0);
session.setAttribute("result",t);//模块队列存入session response.setHeader("Refresh","2;module.jsp");//转到模块显示
}
}catch(Exception r){ out.print("fail"+"\n");
out.print("return to select what you need");
response.setHeader("Refresh","2;addmodule.jsp");//转到模块显示
}
}
```

### 3.2.7 管理员注册

```c++
protected void doGet(HttpServletRequest request, HttpServletResponse response)
throws ServletException, IOException
{
    // TODO Auto-generated method stub response.setContentType("text/html;charset=UTF-8"); PrintWriter out=response.getWriter(); request.setCharacterEncoding("UTF-8");
    byte b1[] = request.getParameter("uname").getBytes("UTF-8");
    String userid = new String(b1, "UTF-8");
    byte b2[] = request.getParameter("pwd").getBytes("UTF-8");
    String pwd = new String(b2);
    byte b3[] = request.getParameter("qpwd").getBytes("UTF-8");
    String qpwd = new String(b3);
    if(pwd.equals(qpwd))
    {
        DB.getDb().insertAdmin(userid, pwd); //将注册信息插入数据库out.print(" 注 册 成 功 ， 两 秒 后 跳 转 ！！！"); response.setHeader("Refresh","2;usermanagement.jsp");
    }
    else
    {
        out.print("注册失败，请检查用户名或密码是否正确，两秒后跳转！！！");
        response.setHeader("Refresh", "2;usermanagement.jsp");
    }
}
```



### 3.2.8 修改密码功能

```c++
protected void doPost(HttpServletRequest request, HttpServletResponse response)
throws ServletException, IOException
{
    // TODO Auto-generated method stub
    response.setContentType("text/html;charset=UTF-8");
    PrintWriter out = response.getWriter();
    request.setCharacterEncoding("UTF-8");
    HttpSession session = request.getSession();
    String userid = (String) session.getAttribute("userid");
    String user = (String) session.getAttribute("identity");
    String pass = DB.getDb().getPassword(userid);
    byte b1[] = request.getParameter("opwd").getBytes("UTF-8");
    String opwd = new String(b1, "UTF-8");
    byte b2[] = request.getParameter("npwd").getBytes("UTF-8");
    String npwd = new String(b2, "UTF-8");
    byte b3[] = request.getParameter("epwd").getBytes("UTF-8");
    String epwd = new String(b3, "UTF-8");
    if(!pass.equals(""))
    {
        if(pass.equals(opwd))
        {
            DB.getDb().delete(3, userid);
            if(npwd.equals(epwd))
            {
                //	System.out.println(user);
                if(user.equals("user"))
                {
                    DB.getDb().insertUser(userid, npwd);
                    out.println("更新成功，两秒后跳转至登录界面！！");
                    response.setHeader("Refresh", "2;login.jsp");
                }
                else if(user.equals("admin"))
                {
                    DB.getDb().insertAdmin(userid, npwd);
                    out.println("更新成功，两秒后跳转至登录界面！！");
                    response.setHeader("Refresh", "2;login.jsp");
                }
            }
            else
            {
                out.print("更新失败，请检查新密码与确认密码是否相同，两秒后跳  转至修改页面！！！");
                response.setHeader("Refresh", "2;account_info.jsp");
            }
        }
        ");
    }
    else
    {
        //密码输入错误	调回重新选择
        out.print("更新失败，请检查旧密码是否输入正确，两秒后跳转至修改界面！！！
            response.setHeader("Refresh", "2;account_info.jsp");
        }
    }
```

## 3.3 数据库设计

### 3.3.1 概念模型

实体-关系图：

![](https://www.writebug.com/myres/static/uploads/2021/10/26/1a3f5040a0a4f169f91f73d09f0a7100.writebug)

### 3.3.2数据模型

数据库统一采用MySQL，数据库名为 test，该数据库下有四个表，分别为 message 表，module表，topic 表，user 表。

![](https://www.writebug.com/myres/static/uploads/2021/10/26/08e6f1f3049af9cf6184cda182ce639e.writebug)

图 3.2 test 数据库

![](https://www.writebug.com/myres/static/uploads/2021/10/26/e1d21c9775dbf215e69f2bf0a416afd3.writebug)

- user 表：userid 即为用户名，pwd 即为密码，admin 为管理员，user 为用户。

图 3.3 user 表

- topic 表：mnum 用于和 module 表匹配，tnum 用于和 message 表匹配，tname 为该 topic 的主要内容，owner 用于和 user 匹配显示改 topic 的发起人。

![](https://www.writebug.com/myres/static/uploads/2021/10/26/3117306a18cb7155e385c7ddd711f88d.writebug)

图 3.4 topic 表

- module 表：mnum 为给当前 module 分配的编号用于和 topic 匹配，mname 为该 module

的主要内容。

![](https://www.writebug.com/myres/static/uploads/2021/10/26/69b4f2e68cf1aae5695d3eb782e4e4bf.writebug)

图 3.5 module 表

![](https://www.writebug.com/myres/static/uploads/2021/10/26/1bc23469c015df24398d6ede1d199937.writebug)

- message 表：mid 为给当前留言分配的 id，tnum 是为了和 topic 匹配，mnum 是为了和module 表匹配，userid 是为了和 user 表匹配，date 为发表留言的日期，content 为该留言的内容。

图 3.6 message 表

### 3.3.3系统流程图

![](https://www.writebug.com/myres/static/uploads/2021/10/26/d19c2822d3b37ba5f9bc6797f7bdcbaa.writebug)

# 四、系统实现

## 4.1登录功能

登录页面：输入用户名密码，然后在下方选择普通用户或者管理员即可登录。

![](https://www.writebug.com/myres/static/uploads/2021/10/26/a4631a613b8cbce53bb43b1f11e5b4b0.writebug)

图 4.1 登陆页面

## 4.2注册功能

![](https://www.writebug.com/myres/static/uploads/2021/10/26/24df8ef9e8e6cad8cc745e3bfe5bf73f.writebug)

注册页面：输入用户名，密码以及确认密码后即可注册。

图 4.2 注册页面

## 4.3管理员登录

![](https://www.writebug.com/myres/static/uploads/2021/10/26/b34c60a40e93e8ad24870c0e92620f40.writebug)

管理员登陆页面：（可删除任意模块）

图 4.3 管理员登录页面

## 4.4添加模块及模块查询：

![](https://www.writebug.com/myres/static/uploads/2021/10/26/e6045214b9b8484dff48f1ff15a5325a.writebug)

图 4.4 添加模块及模块查询页面

添加模块显示：

![](https://www.writebug.com/myres/static/uploads/2021/10/26/c6833f48ac12c1ec1ec88e708626b695.writebug)

图 4.5 添加模块成功页面

![](https://www.writebug.com/myres/static/uploads/2021/10/26/dece679467ec0a7f990c5ef1421e5a1b.writebug)

查询模块显示：

图 4.6 查询成功界面

## 4.5添加话题页面：

![](https://www.writebug.com/myres/static/uploads/2021/10/26/c6833f48ac12c1ec1ec88e708626b695.writebug)

图 4.7 添加话题页面

![](https://www.writebug.com/myres/static/uploads/2021/10/26/0918a760c37671f936d790370e432dcf.writebug)

## 4.6添加留言页面：

图 4.8 添加留言页面

![](https://www.writebug.com/myres/static/uploads/2021/10/26/4c450312d45522fa672f304a270a6c86.writebug)

- 模块内部话题页面：（可删除任意话题）

- 图 4.9 模块内部话题页面

- 话题内部留言页面：（可删除任意留言）

![](https://www.writebug.com/myres/static/uploads/2021/10/26/3098600fb03046b845e944cff6e52e2a.writebug)

图 4.10 话题内部留言页面

![](https://www.writebug.com/myres/static/uploads/2021/10/26/a3982073ae98b046b74426897d51d6cb.writebug)

## 4.7增加管理员及删除用户：

图 4.11 增加管理员及删除用户页面

修改密码：

![](https://www.writebug.com/myres/static/uploads/2021/10/26/f0d583170ecebcac70332628890bcb45.writebug)

图 4.12 修改密码页面

- 普通用户登录

![](https://www.writebug.com/myres/static/uploads/2021/10/26/936ba88f5aebd679bf5dc32139f973d1.writebug)

普通用户登录页面：

图 4.13 普通用户登录页面

![](https://www.writebug.com/myres/static/uploads/2021/10/26/d24978d4c50b12312fd8d26d09a5fbfe.writebug)

修改密码示例：将用户李四的密码改为 2020 修改前：

图 4.14 修改密码前的 user 表

![](https://www.writebug.com/myres/static/uploads/2021/10/26/1a5d3bbacb89f9b8fd9a95500c106bb6.writebug)

修改后：

图 4.15 修改密码后的 user 表

