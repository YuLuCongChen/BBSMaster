package servelet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Database.DB;

/**
 * Servlet implementation class adminregister
 */
@WebServlet("/adminregister")
public class adminregister extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public adminregister() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out=response.getWriter();
		request.setCharacterEncoding("UTF-8"); 
		byte b1[]=request.getParameter("uname").getBytes("UTF-8");
		String userid=new String(b1,"UTF-8");
		byte b2[]=request.getParameter("pwd").getBytes("UTF-8");
		String pwd=new String(b2);
		byte b3[]=request.getParameter("qpwd").getBytes("UTF-8");
		String qpwd=new String(b3);
		if(pwd.equals(qpwd)) {
			DB.getDb().insertAdmin(userid, pwd);//将注册信息插入数据库
			out.print("注册成功，两秒后跳转！！！");
			response.setHeader("Refresh","2;usermanagement.jsp"); 	
		}
		else {
			out.print("注册失败，请检查用户名或密码是否正确，两秒后跳转！！！");
			response.setHeader("Refresh","2;usermanagement.jsp");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
