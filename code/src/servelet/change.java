package servelet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Database.DB;

/**
 * Servlet implementation class change
 */
@WebServlet("/change")
public class change extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public change() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out=response.getWriter();
		request.setCharacterEncoding("UTF-8"); 
		
		HttpSession session=request.getSession();  
		String userid=(String)session.getAttribute("userid");
		
		
		//String pwd=(String)session.getAttribute("pwd");
		
		String user=(String)session.getAttribute("identity");
		
		String pass=DB.getDb().getPassword(userid);
		
		byte b1[]=request.getParameter("opwd").getBytes("UTF-8");
		String opwd=new String(b1,"UTF-8");
		byte b2[]=request.getParameter("npwd").getBytes("UTF-8");
		String npwd=new String(b2,"UTF-8");
		byte b3[]=request.getParameter("epwd").getBytes("UTF-8");
		String epwd=new String(b3,"UTF-8");
		
//		System.out.println(npwd);
//		System.out.println("我是新密码");
//		System.out.println(epwd);
//		System.out.println("我是确认密码");
		
		if(!pass.equals("")) {
			if(pass.equals(opwd)) {
				DB.getDb().delete(3, userid);
				if(npwd.equals(epwd)) {
//					System.out.println(user);
					   if(user.equals("user")) {
						   DB.getDb().insertUser(userid, npwd);
						   out.println("更新成功，两秒后跳转至登录界面！！");
						   response.setHeader("Refresh","2;login.jsp");
					   }
					   else if(user.equals("admin")) {
						   DB.getDb().insertAdmin(userid, npwd);
						   out.println("更新成功，两秒后跳转至登录界面！！");
						   response.setHeader("Refresh","2;login.jsp");		
					   }
				}
				else {
					out.print("更新失败，请检查新密码与确认密码是否相同，两秒后跳转至修改页面！！！");
					response.setHeader("Refresh","2;account_info.jsp");
				}
			}
			else {
			//密码输入错误  调回重新选择
			out.print("更新失败，请检查旧密码是否输入正确，两秒后跳转至修改界面！！！");
			response.setHeader("Refresh","2;account_info.jsp");
			}
		}
	}	
}
