package servelet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Database.*;


/**
 * Servlet implementation class content
 */
@WebServlet("/content")
public class content extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public content() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out=response.getWriter();
		request.setCharacterEncoding("UTF-8"); 
		HttpSession session=request.getSession();  
		if(request.getParameter("content")!=null)//插入留言
		{
		byte b1[]=request.getParameter("content").getBytes("UTF-8");//获取留言内容
		String content=new String(b1,"UTF-8");
		Message m = new Message();
		m.setContent(content);
		m.setDate(new Date()); 
		m.setUserid((String) session.getAttribute("userid"));//通过session获取登陆用户id
		m.setMnum((int) session.getAttribute("mnum"));
		m.setTnum((int) session.getAttribute("tnum"));
		DB.getDb().addInfo(m);//消息插入数据库
		response.sendRedirect("result?pos=message");//刷新
		}
		if(request.getParameter("tname")!=null)//插入话题
		{
			byte b1[]=request.getParameter("tname").getBytes("UTF-8");//获取话题名
			String content=new String(b1,"UTF-8");
			Topic m = new Topic();
			m.setTname(content);
			m.setOwner((String) session.getAttribute("userid")); 
			m.setTnum(Math.abs((int) new Date().getTime()));
			m.setMnum((int) session.getAttribute("mnum"));
			DB.getDb().addTopic(m);//话题插入数据库
			response.sendRedirect("result?pos=topic");//刷新
		}
		if(request.getParameter("mname")!=null)//插入模块
		{
			byte b1[]=request.getParameter("mname").getBytes("UTF-8");//获取模块名
			String content=new String(b1,"UTF-8");
			Module m = new Module();
			m.setMname(content);
			m.setMnum(Math.abs((int) new Date().getTime()));
			DB.getDb().addModule(m);//模块插入数据库
			response.sendRedirect("result?pos=module");//刷新
		}
	}

}
