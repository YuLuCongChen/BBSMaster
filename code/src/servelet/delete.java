package servelet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Database.DB;
import Database.Message;

/**
 * Servlet implementation class delete
 */
@WebServlet("/delete")
public class delete extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public delete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out=response.getWriter();
		String m_id=request.getParameter("m_id");
		String tnum=request.getParameter("tnum");
		String mnum=request.getParameter("mnum");
		String userid=request.getParameter("userid");
		if(m_id!=null)
		{
			DB.getDb().delete(0,m_id);//删除消息	
			response.sendRedirect("result?pos=message");
			
		}
		if(tnum!=null)
		{
			DB.getDb().delete(1,tnum);//删除话题	
			response.sendRedirect("result?pos=topic");
		}
		if(mnum!=null)
		{
			DB.getDb().delete(2,mnum);//删除模块
			response.sendRedirect("result?pos=module");
		}
		if(userid!=null) {
			DB.getDb().delete(3,userid);//删除用户
			out.print("删除成功，两秒后跳转！！！");
			response.setHeader("Refresh","2;usermanagement.jsp");
		}
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
