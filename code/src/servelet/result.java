package servelet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Database.DB;
import Database.Message;
import Database.Module;
import Database.Topic;

/**
 * Servlet implementation class result
 */
@WebServlet("/result")
public class result extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public result() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html;charset=UTF-8");
		HttpSession session=request.getSession();
		String pos=(String)request.getParameter("pos");//从url中获取pos值
		if(pos!=null)
		{
		switch(pos)
		{
		case "topic"://显示话题
		{
		ArrayList<Module> t;
		String _mnum=request.getParameter("mnum");//从url中获取选择的模块的模块号
		int mnum=0;
		if(_mnum!=null)
		{
			mnum=Integer.parseInt(_mnum);
			session.setAttribute("mnum",mnum );
		}
		else
		{
			mnum=(int)session.getAttribute("mnum");//如果url中获取失败，则从session中获取选择的模块的模块号
		}
		session.setAttribute("module", "true");
		t=DB.getDb().find("topic",mnum, 0);
		session.setAttribute("result",t );//话题队列存入session
		response.sendRedirect("topic.jsp");//转到话题显示
		break;
		}
		case "message"://显示留言
			{
				ArrayList<Message> ly;
				String _tnum=request.getParameter("tnum");//从url中获取选择的模块的模块号
				int tnum=0;
				if(_tnum!=null)
				{
					tnum=Integer.parseInt(_tnum);//留言队列存入session
					session.setAttribute("tnum",tnum );//转到留言显示
				}
				else
				{
					tnum=(int)session.getAttribute("tnum");//如果url中获取失败，则从session中获取选择的话题的话题号
				}
				
				int mnum=(int)session.getAttribute("mnum");//从session中获取选择的模块的模块号
				
				ly=DB.getDb().find("message",mnum,tnum);
				session.setAttribute("result", ly);
				response.sendRedirect("result.jsp"); 
				
				break;
			}
		case "module"://显示模块
		{
		ArrayList<Module> t;
		t=DB.getDb().find("module", 0, 0);
		session.setAttribute("result",t );;//模块队列存入session
		response.sendRedirect("module.jsp");//转到模块显示
		break;
		}
		}
		
		}
		else
		{
			ArrayList<Module> t;
			t=DB.getDb().find("module", 0, 0);
			session.setAttribute("result",t );
			response.sendRedirect("module.jsp");
		}
		
		
		
	}

	private String String(Object attribute) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
