package servelet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Database.DB;

/**
 * Servlet implementation class login
 */
@WebServlet("/login")
public class login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8"); 
		PrintWriter out=response.getWriter();
		byte b1[]=request.getParameter("uname").getBytes("UTF-8");
		String userid=new String(b1,"UTF-8");
		byte b2[]=request.getParameter("pwd").getBytes("UTF-8");
		String pwd=new String(b2);
		byte b3[]=request.getParameter("identity").getBytes("UTF-8");
		String identity=new String(b3);
		if(DB.getDb().checkUser(userid, pwd,identity)==null)//进行身份验证，如果返回null说明登录失败，转回登陆界面
			{
			HttpSession session=request.getSession();  
			session.setAttribute("message", "登录失败！");
			response.sendRedirect("login.jsp");
			}
		else
		{
			HttpSession session=request.getSession();   
			session.setAttribute("userid",userid);//登陆成功，在session中存入已经登陆用户名
			session.setAttribute("identity",identity);//登陆成功，在session中存入身份
			Cookie cookie1=new Cookie("uname",userid);//用户名和密码存入cookie
			Cookie cookie2=new Cookie("pwd",pwd);
			response.addCookie(cookie1);
			response.addCookie(cookie2);
			session.setAttribute("success","登陆成功！");
			session.setAttribute("message", null);
			if(session.getAttribute("identity").equals("admin")) {
				response.sendRedirect("Homepage_admin.jsp");	
			}
			else {
				response.sendRedirect("Homepage_user.jsp"); 	
			}
		}
	}

}
