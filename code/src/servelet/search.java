package servelet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Database.DB;
import Database.Module;
import Database.Topic;

/**
 * Servlet implementation class search
 */
@WebServlet("/search")
public class search extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public search() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8"); 
		//response.setContentType("text/html;charset=UTF-8");
		HttpSession session=request.getSession();
		PrintWriter out=response.getWriter();
		String sname=request.getParameter("sname");
		String tname=request.getParameter("tname");
		//System.out.println(sname);
		
		try{			
			if(tname!=null) {			   
			    ArrayList<Topic> t;
				t=DB.getDb().search(tname,1);
				session.setAttribute("result",t);//模块队列存入session				
			
			    response.setHeader("Refresh","2;topic.jsp");//转到话题显示
				
				}
			
		    if(sname!=null){
				ArrayList<Module> t;
				t=DB.getDb().search(sname,0);
				session.setAttribute("result",t);//模块队列存入session
				
			   response.setHeader("Refresh","2;module.jsp");//转到模块显示				
		
		    }	
		}catch(Exception r){
		out.print("fail"+"\n");
		out.print("return to select what you need");
		
		response.setHeader("Refresh","2;addmodule.jsp");//转到模块显示
		}
			
	}

}
