package Database;

import java.util.Date;

public class Message {//消息类
private int Mnum;//模块号
private int Tnum;//话题号
private int m_id;//消息id
private String userid;//用户id
private Date date;//日期
private String _date;//日期字符串
private String content;//消息内容
public String getUserid() {
	return userid;
}
public void setUserid(String userid) {
	this.userid = userid;
}
public Date getDate() {
	return date;
}
public void setDate(Date date) {
	this.date = date;
}
public String getContent() {
	return content;
}
public void setContent(String content) {
	this.content = content;
}
public int getMnum() {
	return Mnum;
}
public void setMnum(int mnum) {
	Mnum = mnum;
}
public int getTnum() {
	return Tnum;
}
public void setTnum(int tnum) {
	Tnum = tnum;
}
public int getM_id() {
	return m_id;
}
public void setM_id(int m_id) {
	this.m_id = m_id;
}
public String get_date() {
	return _date;
}
public void set_date(String _date) {
	this._date = _date;
}

}
