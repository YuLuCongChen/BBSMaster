package Database;

import java.lang.reflect.InvocationTargetException;

import java.sql.*;
import java.util.ArrayList;
public class DB{
    
	String driverName="com.mysql.cj.jdbc.Driver";
	String userName="root";
	String userPasswd="root";
	String dbName="test";
	String url="jdbc:mysql://localhost:3306/"+dbName+"?&serverTimezone=GMT%2B8";
	Connection con=null; 
	Statement s; 
	ResultSet rs;
	private static DB db=null;//单件模式
	
	private DB() throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException
	{
	try//与数据库建立连接
		{
			Class.forName(driverName).getDeclaredConstructor().newInstance();
		}catch(ClassNotFoundException e)
		{System.out.print("Error loading Driver,不能加载驱动程序！");}
		try
		{con=DriverManager.getConnection(url,userName,userPasswd);
		}
		catch(SQLException er)
		{System.out.print("Error getConnection,不能连接数据库！");}
	}
  
//  用于执行各种SQL语句的方法
  private ResultSet execSQL(String sql , Object ...args) 
          throws SQLException
  {
      PreparedStatement pstmt = con.prepareStatement(sql) ;
//      为PreparedStatement对象设置SQL参数
      for (int i = 0 ; i < args.length ; ++ i)
      {
          pstmt.setObject(1 + i, args[i]) ; 
      }
//      运行
      pstmt.execute() ; 
      return  pstmt.getResultSet(); 
  }
//检查用户登录信息
  public User checkUser(String userid , String pwd, String identity)
  {
      try
      {
          String sql = "select * from user where userid = ? and  pwd = ? and identity=?" ;
          ResultSet rs = execSQL(sql,userid,pwd,identity) ; 
          User user = new User() ; 
          while (rs.next())
          {    
              user.setUserid(rs.getString("userid"));
              user.setPwd(rs.getString("pwd"));
              return user ;
          } 
          return null ;
      }
      catch(Exception e)
      {
          e.printStackTrace();
          return null ; 
      }
  }
//插入留言
  public boolean addInfo(Message ly){
      try{
    	 
    	  int num=Math.abs((int) ly.getDate().getTime());//利用当前时间给留言编号
          String sql = "insert into message(m_id,mnum,tnum,userid,date,content) values(?,?,?,?,?,?)" ; 
          execSQL(sql,num+1,ly.getMnum(),ly.getTnum(),ly.getUserid(),ly.getDate(),ly.getContent());
          return true;
      }catch(Exception e){
          e.printStackTrace();
          return false;
      }
  }
//插入话题
  public boolean addTopic(Topic ly){
      try{ 
          String sql = "insert into topic(mnum,tnum,tname,owner) values(?,?,?,?)" ; 
          execSQL(sql,ly.getMnum(),ly.getTnum(),ly.getTname(),ly.getOwner());
          return true;
      }catch(Exception e){
          e.printStackTrace();
          return false;
      }
  }
//插入模块
  public boolean addModule(Module ly){
      try{
    	 
    	  
          String sql = "insert into module(mnum,mname) values(?,?)" ; 
          execSQL(sql,ly.getMnum(),ly.getMname());
          return true;
      }catch(Exception e){
          e.printStackTrace();
          return false;
      }
  }
  //由模块id获得模块名
  public String getMname(int mnum){
	  ResultSet rs=null;
	  String s="";
      try{ 	 
    	  
          String sql = "select mname from module where mnum=?" ; 
          rs=execSQL(sql,mnum);
          while(rs.next()) {
        	  s=rs.getString(1);
        	  
          }
      }catch(Exception e){
          e.printStackTrace();
      }
      return s;
  }
  //由用户名得到密码
  public String getPassword(String mnum) {
	  ResultSet rs=null;
	  String pass="";
	  String sql="select pwd from user where userid=?";
	  
	  try {
		rs=execSQL(sql,mnum);
		while(rs.next()) {
	    	  pass=rs.getString(1);    	  
	      }
	} catch (SQLException e) {
		// TODO 自动生成的 catch 块
		e.printStackTrace();
	}  
	  return pass;
  }
  
  //查询功能
  public ArrayList search(String mname,int a){
	   
	    if(a==0)//模块查询
	    {
	    ArrayList<Module> end= new ArrayList() ;   
	 	    
	    String sa="%"+mname+"%";
	    
		//System.out.println(sa+"DB.java");	     
	    String sql="select * from module where mname like ?";//模糊查询
	    // a= 0 1 搜索 对应属性
	    ResultSet rs=null;
		try {
			PreparedStatement pstmt = con.prepareStatement(sql) ;
			 pstmt.setObject(1,sa);
			 rs=pstmt.executeQuery();
			 
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		} 		  
		try {			
			while(rs.next())
			{
			  //System.out.println(mname);
			  Module t = new Module(); 
			  t.setMnum(Integer.parseInt(rs.getString("mnum")));
			  t.setMname(rs.getString("mname"));
			  end.add(t) ; 			  	
            }
		} catch (NumberFormatException | SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}	    
	    return end;
	    }
	        
	    if(a==1)//话题查询
	    {
	    ArrayList<Topic> end= new ArrayList() ;   
	 	    
	    String sa="%"+mname+"%";
	    
	    //System.out.println(sa+"DB.java");
	    
	    String sql="select * from topic where tname like ?";
	
	    ResultSet rs=null;
		try {
			 PreparedStatement pstmt = con.prepareStatement(sql);
			 pstmt.setObject(1,sa);
			 rs=pstmt.executeQuery();
			 
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		} 		  
		try {
			
			while(rs.next())
			{
			  //System.out.println(mname);
			  Topic t = new Topic() ; 
	              t.setMnum(Integer.parseInt(rs.getString("mnum")));
	              t.setTnum(Integer.parseInt(rs.getString("tnum")));
	              t.setTname(rs.getString("tname"));
	              t.setOwner(rs.getString("owner"));
	              end.add(t) ; 	  
			  //System.out.println(t);
			
      }
		} catch (NumberFormatException | SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	    
	    return end;
	    }
	    
	    return null;
	    
  }
//删除功能
  public void delete(int choice,String m_id)
  {switch(choice)
	  {
	  case 0:{//删除留言
		  try{
			  execSQL("delete from message where m_id=?",m_id);
		  }catch(Exception e){
			  e.printStackTrace();
			  }break;}
	  case 1://删除话题
	  {
		  try{
			  execSQL("delete from topic where tnum=?",m_id);
		  }catch(Exception e){
			  e.printStackTrace();
			  }break;}
	  case 2://删除模块
	  {
		  try{
			  execSQL("delete from module where mnum=?",m_id);
		  }catch(Exception e){
			  e.printStackTrace();
			  }break;}
	  case 3://删除用户
	  {
		  try{
			  execSQL("delete from user where userid=?",m_id);			  
		  }catch(Exception e){
			  e.printStackTrace();
			  }break;}
	  }
	  }
  
//  插入用户
  public boolean insertUser(String username,String password){
      try{
          String sql = "insert into user(userid,pwd,identity) values(?,?,?)" ; 
          execSQL(sql ,username , password,"user") ;  
          return true;
      }catch(Exception e){
          e.printStackTrace();
          return false;
      }
  }
  //插入管理员
  public boolean insertAdmin(String username,String password){
      try{
          String sql = "insert into user(userid,pwd,identity) values(?,?,?)" ; 
          execSQL(sql ,username , password,"admin") ;  
          return true;
      }catch(Exception e){
          e.printStackTrace();
          return false;
      }
  }
//  获取模块、话题、留言。
  public ArrayList find(String object,int Mnum,int Tnum)
  {
       
       try 
       {
    	   switch(object)
    	   {
    	   case "module": //获取模块
    		   {
    			   String sql = "select * from module order by mnum " ; 
    			   ResultSet rs = execSQL(sql) ; 
    			   ArrayList<Module> arrayList = new ArrayList() ; 
    			   while(rs.next())
    	           {
    	        	  Module t = new Module() ; 
    	              t.setMnum(Integer.parseInt(rs.getString("mnum")));
    	              t.setMname(rs.getString("mname"));
    	              arrayList.add(t); 
    	           }
    	           return arrayList; 
    		   }
    		   
    	   case "topic"://根据模块号获取话题
    	   		{
    	   			String sql = "select * from topic where mnum=? order by tnum" ; 
    	   			ResultSet rs = execSQL(sql,Mnum);
    	   			ArrayList<Topic> arrayList = new ArrayList(); 
    	   			while(rs.next())
     	           {
     	        	  Topic t = new Topic() ; 
     	              t.setMnum(Integer.parseInt(rs.getString("mnum")));
     	              t.setTnum(Integer.parseInt(rs.getString("tnum")));
     	              t.setTname(rs.getString("tname"));
     	              t.setOwner(rs.getString("owner"));
     	              arrayList.add(t) ; 
     	           }
     	           return arrayList ; 
    	   		}
    	   case "message"://根据模块号、话题号获取话题
    	   		{
    	   			ArrayList<Message> arrayList = new ArrayList() ; 
    	   			String sql = "select * from message where mnum=? and tnum=? order by m_id" ; 
    	   			ResultSet rs = execSQL(sql,Mnum,Tnum); 
    	   			while(rs.next())
    	   			{
    	   				Message ly = new Message() ;
    	   				ly.setM_id(Integer.parseInt(rs.getString("m_id")));
    	   				ly.setMnum(Integer.parseInt(rs.getString("mnum")));
    	   				ly.setTnum(Integer.parseInt(rs.getString("tnum")));
    	   				ly.setUserid(rs.getString("userid"));
    	   				ly.set_date(rs.getString("date")) ; 
    	   				ly.setContent(rs.getString("content"));
    	   				arrayList.add(ly) ; 
    	   			}
    	   				return arrayList ; 
    	   		}
           
    	   }
    	   
       }
       catch(Exception e)
       {
           e.printStackTrace() ; 
           return null ; 
       }
	return null;
  }

public static DB getDb() {//单件模式
	if(db==null)
		try {
			db=new DB();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	return db;
}

}
