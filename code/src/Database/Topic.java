package Database;

public class Topic {//话题类
private String Tname;//话题名
private int Tnum;//话题号
private String owner;//话题拥有者
private int Mnum;//模块号
public String getTname() {
	return Tname;
}
public void setTname(String tname) {
	Tname = tname;
}
public String getOwner() {
	return owner;
}
public void setOwner(String owner) {
	this.owner = owner;
}
public int getTnum() {
	return Tnum;
}
public void setTnum(int tnum) {
	Tnum = tnum;
}
public int getMnum() {
	return Mnum;
}
public void setMnum(int mnum) {
	Mnum = mnum;
}

}
