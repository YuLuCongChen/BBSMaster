<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>登陆到校园BBS</title>
<link rel="stylesheet" href="./layui/css/layui.css">
</head>
<!-- background存放登陆页面背景图片 -->
<!-- style="background-repeat: no-repeat; background-size: 100% 30%; background-attachment: fixed;" -->
<body>
<script src="./layui/layui.js"></script>
	<% 
		Cookie[] cookies = request.getCookies();//从cookie中获取登陆用户名和密码
	if (cookies != null) {
		for (Cookie cookie : cookies) {
			if (cookie.getName().equals("uname")) {
		uname = cookie.getValue();
			}
			if (cookie.getName().equals("pwd")) {
		pwd = cookie.getValue();
			}

		}
	}
	%>
	<!-- 登录失败时弹出错误消息 -->
	<%
		Object message = session.getAttribute("message");
	if (message != null&&!"".equals(message)) {
		session.setAttribute("message","");
	%>
	<script>
		layui.use('layer', function() {
			var layer = layui.layer;
			layer.msg("登录失败! 请检查用户名或密码是否正确!");
		});
	</script>
	
	<%
		}
	%>
	<!-- 长大logo -->
	<!-- class="layui-nav" layui-bg-cyan lay-filter="" -->
	<ul class="layui-nav" layui-bg-cyan lay-filter="">
		<li class="layui-nav-item" style="padding:5px;left:200px">
			<img  src="chd_logo3.png">
		</li>
		<li class="layui-nav-item" style="position = absolute; left:700px">
		<a href="http://www.chd.edu.cn/">长安大学官网</a>
		</li>
		<li class="layui-nav-item" style="position = absolute; left:700px">
		<a href="http://ids.chd.edu.cn/authserver/login?service=http%3A%2F%2Fportal.chd.edu.cn%2F">统一身份认证</a>
		</li>
		<li class="layui-nav-item layui-this" style="position = absolute; left:710px">
	       校园BBS登录
		</li>
	</ul>
	<br>
	<hr class="layui-bg-gray">
	<br>
	<!-- 登陆和注册卡片-->
	<div class="layui-row">
		<div class="layui-col-xs-offset0" align="center">
			<div class="layui-tab layui-tab-card"
				style="width: 350px; background-color: white">
				<!-- 卡片头选项-->
				<ul class="layui-tab-title">
					<li class="layui-this">登陆</li>
					<li>注册</li>
				</ul>
				<!-- 卡片内容-->
				<div class="layui-tab-content">
					<!-- 登陆表 -->
					<div class="layui-tab-item layui-show">
						<%!String uname, pwd;//用户名和密码%>
						<div class="layui-anim layui-anim-up">
							<fieldset class="layui-elem-field layui-field-title"
								style="margin-top: 20px;">
								<legend>---------登录到校园BBS---------</legend>
							</fieldset>
							<!-- 登陆交给login.java处理 -->
							<form action="login" class="layui-form layui-form-pane"
								method=post name=form>

								<div class="layui-form-item">
									<label class="layui-form-label">用户名</label>
									<div class="layui-input-inline">
										<input type="text" name="uname"
											value="<%=(pwd == null ? "" : uname)%>" 
											required="required" placeholder="请输入用户名"
											autocomplete="off" class="layui-input">
									</div>
								</div>

								<div class="layui-form-item">
									<label class="layui-form-label">密码</label>
									<div class="layui-input-inline">
										<input type="password" name="pwd"
											value="<%=(pwd == null ? "" : pwd)%>" 
										  required="required" placeholder="请输入密码"
											autocomplete="off" class="layui-input">
									</div>
								</div>

								<div class="layui-form-item">
									<input type="radio" name="identity" lay-skin="primary"
										value="user" title="普通用户" checked> <input type="radio"
										name="identity" lay-skin="primary" value="admin" title="管理员">
									<input type="submit" class="layui-btn  layui-btn-radius"
										value="登陆">
								</div>
							</form>
						</div>
					</div>	
					<!-- 注册表 -->
					<div class="layui-tab-item">
						<div class="layui-anim layui-anim-up">
							<fieldset class="layui-elem-field layui-field-title"
								style="margin-top: 20px;">
								<legend>普通用户注册</legend>
							</fieldset>
							<!-- 登陆交给register.java处理 -->
							<form action="register" class="layui-form layui-form-pane"
								method=post name=form>

								<div class="layui-form-item">
									<label class="layui-form-label">用户名</label>
									<div class="layui-input-inline">
										<input type="text" name="uname" placeholder="请输入用户名"
											autocomplete="off" class="layui-input">
									</div>
								</div>

								<div class="layui-form-item">
									<label class="layui-form-label">密码</label>
									<div class="layui-input-inline">
										<input type="password" name="pwd" placeholder="请输入密码"
											autocomplete="off" class="layui-input">
									</div>
								</div>
								
								<div class="layui-form-item">
									<label class="layui-form-label">确认密码</label>
									<div class="layui-input-inline">
										<input type="password" name="qpwd" placeholder="请再次输入密码"
											autocomplete="off" class="layui-input">
									</div>
								</div>

								<div class="layui-form-item">
									<input type="submit"
										class="layui-btn  layui-btn-radius layui-btn-fluid "
										value="注册">
								</div>
							</form>

						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<br>
	<hr class="layui-bg-black">
	<br>
	<p align="center">
	小组成员：姚灿，陈勤，刘丁，宋硕
	</p>
	<br>
	<p align="center">保留所有权利</p>
	
	<script>
		layui.use('form', function() {
			var form = layui.form;
			form.render();
			//监听提交
			form.on('submit(formDemo)', function(data) {
				layer.msg(JSON.stringify(data.field));
				return false;
			});
		});
		layui.use('element', function() {
			var element = layui.element;
		});
	</script>
</body>
</html>