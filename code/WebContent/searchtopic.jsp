<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="./layui/css/layui.css">
</head>
<body>
	<fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
  <legend>话题查询</legend>
</fieldset>
<form class="layui-form" action="search" method="post">
<div class="layui-form-item">
    <label class="layui-form-label">搜索</label>
    <div class="layui-input-inline">
      <input type="text" name="tname" placeholder="请输入关键字" autocomplete="off" class="layui-input">
    </div>
</div>
  <div class="layui-form-item">
    <div class="layui-input-block">
      <button class="layui-btn" type="submit" lay-filter="demo1" lay-submit="">立即提交</button>
      <button class="layui-btn layui-btn-primary" type="reset">重置</button>
    </div>
  </div>
 </form>
 
 <div
		style="position: fixed; left: 0%; bottom: 0px; width: 100%; height: 30px; z-index: 9999;">
		<form>
		<table colspan=3>
		<tr>
		<!-- 返回上一级 -->
		<td align="center"><a href="result?pos=module">
		  <i class="layui-icon layui-icon-left" align="left"
			style="font-size: 20px; color: #009688">上一级</i></a> </td>			
		</tr>
		</table>
		</form>
</div>
 
 <script>
//注意：折叠面板 依赖 element 模块，否则无法进行功能性操作
layui.use('element', function(){
  var element = layui.element;
  
  //…
});
</script>

</body>
</html>