<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*,Database.*"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>密码修改</title>
<link rel="stylesheet" href="./layui/css/layui.css">
</head>
<body>
 <script type = "text/javascript">
  	function check()
  	{
  		if(document.myform.opwd.value == "")
  		{
  			alert("请输入旧密码");
  			return false;
  		}
  		if(document.myform.npwd.value == "")
  		{
  			alert("请输入新密码");
  			return false;
  		}
  		if(document.myform.qpwd.value = "")
  		{
  			alert("请确认新密码");
  			return false;
  		}
  		
  		return true;
  	}
  </script>
<script src="./layui/layui.js"></script>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
  <legend>用户密码修改</legend>
</fieldset>
<form class="layui-form" action="change" method="post" name="myform">
<%!String opwd,npwd,epwd;%>
<div class="layui-form-item">
    <label class="layui-form-label">旧密码</label>
    <div class="layui-input-inline">
      <input type="password" name="opwd" value="<%=opwd==null?"":opwd %>" placeholder="请输入旧密码" autocomplete="off" class="layui-input">
    </div>
    <br><br><br>
    <label class="layui-form-label">新密码</label>
    <div class="layui-input-inline">
      <input type="password" name="npwd" value="<%=npwd==null?"":npwd %>" placeholder="请输入新密码" autocomplete="off" class="layui-input">
    </div>
    <br><br><br>
    <label class="layui-form-label">确认密码</label>
    <div class="layui-input-inline">
      <input type="password" name="epwd" value="<%=epwd==null?"":epwd %>" placeholder="请再次输入新密码" autocomplete="off" class="layui-input">
    </div>
</div>
  <div class="layui-form-item">
    <div class="layui-input-block">
      <button class="layui-btn" type="submit" lay-filter="demo1" lay-submit="" onclick = "return check()">立即提交</button>
      <button class="layui-btn layui-btn-primary" type="reset">重置</button>
    </div>
  </div>
  </form>
     
 <script>
//注意：折叠面板 依赖 element 模块，否则无法进行功能性操作
layui.use('element', function(){
  var element = layui.element;
  
  //…
});

</script>
</body>
</html>