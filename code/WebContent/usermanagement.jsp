<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*,Database.*"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>用户管理</title>
<link rel="stylesheet" href="./layui/css/layui.css">
</head>
<body>
<script src="./layui/layui.js"></script>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
  <legend>增加管理员</legend>
</fieldset>
<form class="layui-form" action="adminregister" method="post">
<%!String uname,pwd,qpwd; %>
<div class="layui-form-item">
    <label class="layui-form-label">用户名</label>
    <div class="layui-input-inline">
      <input type="text" name="uname" value="<%=uname==null?"":uname %>" placeholder="请输入用户名" autocomplete="off" class="layui-input">
    </div>
    <br><br><br>
    <label class="layui-form-label">密码</label>
    <div class="layui-input-inline">
      <input type="password" name="pwd" value="<%=pwd==null?"":pwd %>" placeholder="请输入密码" autocomplete="off" class="layui-input">
    </div>
    <br><br><br>
    <label class="layui-form-label">确认密码</label>
    <div class="layui-input-inline">
      <input type="password" name="qpwd" value="<%=qpwd==null?"":qpwd %>" placeholder="请再次输入密码" autocomplete="off" class="layui-input">
    </div>
</div>
  <div class="layui-form-item">
    <div class="layui-input-block">
      <button class="layui-btn" type="submit" lay-filter="demo1" lay-submit="">立即提交</button>
      <button class="layui-btn layui-btn-primary" type="reset">重置</button>
    </div>
  </div>
  </form>
 
 <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
  <legend>删除用户</legend>
</fieldset>
 <form class="layui-form" action="delete" method="post">
<div class="layui-form-item">
    <label class="layui-form-label">用户名</label>
    <div class="layui-input-inline">
      <input type="text" name="userid"   placeholder="请输入用户名" autocomplete="off" class="layui-input">
    </div>
 </div>
 <div class="layui-form-item">
    <div class="layui-input-block">
      <button class="layui-btn" type="submit" lay-filter="demo1" lay-submit="">立即提交</button>
      <button class="layui-btn layui-btn-primary" type="reset">重置</button>
    </div>
  </div>
 </form>
      
 <script>
//注意：折叠面板 依赖 element 模块，否则无法进行功能性操作
layui.use('element', function(){
  var element = layui.element;
  
  //…
});

</script>
</body>
</html>