<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"  import="java.util.*,Database.*"  %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>添加留言</title>
<link rel="stylesheet" href="./layui/css/layui.css">
</head>
<body> 
<script src="./layui/layui.js"></script>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
  <legend>输入留言</legend>
</fieldset>
<form class="layui-form" action="content" method="post">
<div class="layui-form-item layui-form-text">
    <label class="layui-form-label">留言：</label>
    <div class="layui-input-block">
      <textarea class="layui-textarea" name="content" placeholder="请输入内容"></textarea>
    </div>
  </div>
  <div class="layui-form-item">
    <div class="layui-input-block">
      <button class="layui-btn" type="submit" lay-filter="demo1" lay-submit="">立即提交</button>
      <button class="layui-btn layui-btn-primary" type="reset">重置</button>
    </div>
  </div>
  </form>
<script>
//注意：折叠面板 依赖 element 模块，否则无法进行功能性操作
layui.use('element', function(){
  var element = layui.element;
  
  //…
});

</script>
</body>
</html>