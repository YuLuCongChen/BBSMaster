<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*,Database.*"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
<link rel="stylesheet" href="./layui/css/layui.css">
</head>
<body>
	<script src="./layui/layui.js"></script>
	<div style="padding: 20px; background-color: #F2F2F2;">
		<div class='layui-row layui-col-space15'>
			<%
				String userid = (java.lang.String) session.getAttribute("userid");//获取已经登陆用户id
			ArrayList<Topic> ly;
			ly = (ArrayList<Topic>) session.getAttribute("result");//获取话题队列
			for (int i = 0; i < ly.size(); i++) {
			%>

			<div class="layui-col-xs12">
				<div class="layui-card">
					<div class="layui-card-header">
						<div class="layui-col-xs10">
							<i class="layui-icon layui-icon-face-smile-b"><%=DB.getDb().getMname(ly.get(i).getMnum())==""?"话题":DB.getDb().getMname(ly.get(i).getMnum())%></i>
						</div>
						<div class="layui-col-xs2">
							<i class="layui-icon layui-icon-username"><%=ly.get(i).getOwner()%></i>
						</div>


					</div>
					<div class="layui-card-body">
						<a href="result?tnum=<%=ly.get(i).getTnum()%>&pos=message"><%=ly.get(i).getTname()%></a>
						<%
						//如果当前用户是话题拥有者或是管理员，则可以删除话题
							if (session.getAttribute("userid").equals(ly.get(i).getOwner()) || session.getAttribute("identity").equals("admin")) {
						%>
						<div class="layui-col-xs-offset10">
							<a href="delete?tnum=<%=ly.get(i).getTnum()%>"><i
								class="layui-icon layui-icon-delete"></i>删除</a>
						</div>
						<%
							}
						%>
					</div>

				</div>
			</div>

			<%
				}
			%>
		</div>
		<div
			style="position: fixed; left: 0%; bottom: 0px; width: 100%; height: 30px; z-index: 9999;">
			<div class="layui-col-xs5">
				<a href="result?pos=module"> <i
						class="layui-icon layui-icon-left" align="left"
						style="font-size: 20px; color: #009688">上一级</i></a>
			</div>
			<div class="layui-col-xs5">
				<a href="searchtopic.jsp"> <i 
				class="layui-icon layui-icon-search" align="center"
					style="font-size: 20px; color: #009688">话题查找</i></a>
			</div>
			<div class="layui-col-xs2">
				<a href="addtopic.jsp">
				<i class="layui-icon layui-icon-add-circle" align="center"
					style="font-size: 20px; color: #009688">添加话题</i></a>
			</div>

		</div>
	</div>
	<script>
		//注意：折叠面板 依赖 element 模块，否则无法进行功能性操作
		layui.use('element', function() {
			var element = layui.element;

			//…
		});
	</script>
</body>
</html>