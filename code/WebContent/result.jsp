<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*,Database.Message"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>注册</title>
<link rel="stylesheet" href="./layui/css/layui.css">
</head>
<body>
	<script src="./layui/layui.js"></script>
	<div style="padding: 20px; background-color: #F2F2F2;">
		<div class='layui-row layui-col-space15'>
			<%
				String userid = (java.lang.String) session.getAttribute("userid");
			ArrayList<Message> ly;
			ly = (ArrayList<Message>) session.getAttribute("result");//获取消息队列
			for (int i = 0; i < ly.size(); i++) {
			%>

			<div class="layui-col-xs12">
				<div class="layui-card">
					<div class="layui-card-header">
						<div class="layui-col-xs9">
							<i class="layui-icon layui-icon-username"><%=ly.get(i).getUserid()%></i>
						</div>

						<div class="layui-col-xs3"><%=ly.get(i).get_date()%>
							<%
							//如果当前用户是话留言拥有者或是管理员，则可以删除留言
								if (session.getAttribute("userid").equals(ly.get(i).getUserid()) || session.getAttribute("identity").equals("admin")) {
							%>
							<a href="delete?m_id=<%=ly.get(i).getM_id()%>"> <i
								class="layui-icon layui-icon-delete"></i>
							</a>
							<%
								}
							%>
						</div>
					</div>
					<div class="layui-card-body">
						<%=ly.get(i).getContent()%>

					</div>

				</div>
			</div>

			<%
				}
			%>
		</div>
		<div
			style="position: fixed; left: 0%; bottom: 0px; width: 100%; height: 30px; z-index: 9999;">
			<form>
			<table colspan=3>
			<tr>
			<!-- 返回上一级 -->
			<td align="center"><a href="result?pos=topic">
			  <i class="layui-icon layui-icon-left" align="left"
				style="font-size: 20px; color: #009688">上一级</i></a> </td>
			<td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>	
			<td align="center"><a href="addmessage.jsp">
				<i class="layui-icon layui-icon-add-circle" align="center"
				style="font-size: 20px; color: #009688">添加留言</i>
			</a></td>
			</tr>
			</table>
			</form>
		</div>
	</div>
	<script>
		//注意：折叠面板 依赖 element 模块，否则无法进行功能性操作
		layui.use('element', function() {
			var element = layui.element;

			//…
		});
	</script>
</body>
</html>