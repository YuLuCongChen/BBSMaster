<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*,Database.*"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>注册</title>
<link rel="stylesheet" href="./layui/css/layui.css">
</head>
<body>
	<script src="./layui/layui.js"></script>
	<div style="padding: 20px; background-color: #F2F2F2;">
		<div class='layui-row layui-col-space15'>
			<%
				String userid = (java.lang.String) session.getAttribute("userid");//获取已经登陆用户id
			ArrayList<Module> ly;
			ly = (ArrayList<Module>) session.getAttribute("result");//所有模块组成的队列
			for (int i = 0; i < ly.size(); i++) {
			%>

			<div class="layui-col-xs12">
				<div class="layui-card">
					<!-- 卡片头 -->
					<div class="layui-card-header">
						<div class="layui-col-xs10">
							<i class="layui-icon layui-icon-face-smile-b">模块</i>
						</div>
						<%
							if (session.getAttribute("identity").equals("admin")) {//如果是管理员，就可以删除模块
						%>
						<div class="layui-col-xs-offset11">
							<a href="delete?mnum=<%=ly.get(i).getMnum()%>"><i
								class="layui-icon layui-icon-delete"></i>删除</a>
						</div>
						<%
							}
						%>
					</div>
					<!-- 卡片体，输出模块名 -->
					<div class="layui-card-body">
						<a href="result?mnum=<%=ly.get(i).getMnum()%>&pos=topic"><%=ly.get(i).getMname()%></a>
					</div>
				</div>
			</div>

			<%
				}
			%>
		</div>
		<div
			style="position: fixed; left: 45%; bottom: 0px; width: 100%; height: 30px; z-index: 9999;">
			<a href="searchmodule.jsp"> <i
				class="layui-icon layui-icon-search" align="center"
				style="font-size: 20px; color: #009688"> 模块查找</i></a>
		</div>
		
	</div>	
	
	<script>
		//注意：折叠面板 依赖 element 模块，否则无法进行功能性操作
		layui.use('element', function() {
			var element = layui.element;
			//…
		});
	</script>
</body>
</html>