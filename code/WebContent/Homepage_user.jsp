<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<title>校园BBS</title>
<link rel="stylesheet" href="./layui/css/layui.css">
</head>
<body class="layui-layout-body">
	<!-- 显示成功登陆信息 -->
	<%
		Object success = session.getAttribute("success");
	if (success != null && !"".equals(success)) {
		session.setAttribute("success", "");
	%>
	<script src="./layui/layui.js"></script>
	<script>
    layui.use('layer', function(){
  	var layer = layui.layer;
  	layer.msg("<%=success%>");
		});
	</script>
	<%
		}
	%>
	<!-- 水平导航 -->
	<div class="layui-layout layui-layout-admin">
		<div class="layui-header">
			<!-- logo -->
			<div class="layui-logo">校园BBS</div>
			<!-- 输出用戶名 -->		
		    <ul class="layui-nav layui-layout-right">
            <li class="layui-nav-item">
             <a href="javascript:;"><%=session.getAttribute("userid")%></a>
             <dl class="layui-nav-child">
             <dd><a href="account_info.jsp">修改密码</a></dd>
        </dl>
      </li>
    </ul>
    </div>		
		<!-- 左侧导航区域（可配合layui已有的垂直导航） -->
		<div class="layui-side layui-bg-black">
			<div class="layui-side-scroll">
				<ul class="layui-nav layui-nav-tree" lay-filter="test">
					<li class="layui-nav-item "><a href="result" target="main">查看全部模块 </a></li>
					<li class="layui-nav-item"><a href="account_info.jsp" >修改密码</a></li>
						<li class="layui-nav-item"><a href="login.jsp">退出登录</a></li>
				</ul>
			</div>
		</div>
			
		<!-- 内容主体区域 -->
		<div class="layui-body">
		<!-- 窗口，登陆后显示模块 -->
			<iframe name="main" id="demoAdmin" src="result" frameborder="0"
				style="width: 100%; height: 100%;"></iframe>
		</div>
		<!-- 底部 -->
		<div class="layui-footer">欢迎使用校园BBS</div>
	</div>
	<script>
		//JavaScript代码区域
		layui.use('element', function() {
			var element = layui.element;

		});
	</script>
</body>
</html>